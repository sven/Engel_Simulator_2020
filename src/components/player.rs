#[derive(Debug, Clone)]
pub struct Player {
    pub sanity: f32,
    pub collected_hours: u32,

    pub shifts_completed: u32,
    pub difficulty: f32,
}

impl Player {
    pub fn new() -> Player {
        Player {
            sanity: 1.0,
            collected_hours: 0,
            shifts_completed: 0,
            difficulty: 0.0,
        }
    }
}
