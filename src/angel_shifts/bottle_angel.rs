use crate::colliders;
use crate::components;
use crate::entities;
use crate::resources;
use crate::sprites;
use crate::svg_loader;
use crate::utils;

pub struct BottleAngelShift {
    hours: usize,
    level: String,
}

impl BottleAngelShift {
    pub fn generate(rng: &mut impl rand::Rng) -> BottleAngelShift {
        use rand::seq::SliceRandom;

        let compatible_levels = ["assembly-hall-1.svg", "ccl-ground-level.svg"];

        BottleAngelShift {
            hours: rng.gen_range(1, 3),
            level: compatible_levels.choose(rng).unwrap().to_string(),
        }
    }
}

impl super::AngelShiftImpl for BottleAngelShift {
    fn metadata(&self) -> super::ShiftMetadata {
        super::ShiftMetadata {
            title: "Bottle Angel".to_owned(),
            description: "Collect bottles from all bottle drop points in the designated area."
                .to_owned(),
            hours: self.hours,
        }
    }

    fn level_name(&self) -> &str {
        &self.level
    }

    fn init_gameworld(
        &self,
        world: &mut legion::World,
        resources: &mut legion::Resources,
        schedule_builder: &mut legion::systems::Builder,
        level: &svg_loader::SvgLevel,
    ) {
        // Display objective
        let objective = utils::get_element_by_id::<web_sys::Element>("ingame-objective").unwrap();
        objective.set_inner_html(
            r#"
            <text x="30" y="0" class="stats-label">Collect drop points:</text>
            <text id="ingame-bottle-angel-stats" x="430" y="0" class="stats-number">0/4</text>
        "#,
        );
        let stats =
            utils::get_element_by_id::<web_sys::Element>("ingame-bottle-angel-stats").unwrap();

        entities::create_drop_points(world, level);
        resources.insert(BottleAngelState::new(4, stats));

        schedule_builder
            .add_thread_local(collect_bottledrops_system())
            .add_thread_local(update_bottle_shift_system(self.hours));
    }
}

pub struct BottleAngelState {
    collected_drops: usize,
    drops_in_map: usize,
    stats_element: web_sys::Element,
}

impl BottleAngelState {
    fn new(drops_in_map: usize, stats_element: web_sys::Element) -> BottleAngelState {
        BottleAngelState {
            collected_drops: 0,
            drops_in_map,
            stats_element,
        }
    }

    fn update_stats(&self) {
        self.stats_element.set_text_content(Some(&format!(
            "{}/{}",
            self.collected_drops, self.drops_in_map
        )));
    }
}

#[legion::system]
#[read_component(colliders::Collider)]
#[read_component(components::Matebottledrop)]
#[write_component(components::Sprite)]
pub fn collect_bottledrops(
    world: &mut legion::world::SubWorld,
    cmd: &mut legion::systems::CommandBuffer,
    #[resource] player: &resources::Player,
    #[resource] collision_world: &colliders::CollisionWorld,
    #[resource] bottle_angel_state: &mut BottleAngelState,
) {
    use legion::IntoQuery;

    let collider = <&colliders::Collider>::query()
        .get(world, player.0)
        .unwrap();

    let mut bottledrops = <(&components::Matebottledrop, &mut components::Sprite)>::query();
    for pair in collision_world
        .world
        .proximities_with(collider.handle.unwrap(), false)
        .unwrap()
    {
        if pair.3 != ncollide2d::query::Proximity::Intersecting {
            continue;
        }

        let entity = *collision_world.world.objects.get(pair.1).unwrap().data();
        if let Ok((_, sprite)) = bottledrops.get_mut(world, entity) {
            *sprite = components::Sprite::new(sprites::Sprite::BottleDropPointEmpty);
            cmd.remove_component::<components::Matebottledrop>(entity);
            bottle_angel_state.collected_drops += 1;
            bottle_angel_state.update_stats();
        }
    }
}

#[legion::system]
#[write_component(components::Player)]
pub fn update_bottle_shift(
    #[state] hours_to_award: &mut usize,
    world: &mut legion::world::SubWorld,
    #[resource] player: &mut resources::Player,
    #[resource] bottle_angel_state: &mut BottleAngelState,
    #[resource] game_manager: &mut resources::GameManager,
) {
    use legion::IntoQuery;

    if bottle_angel_state.collected_drops >= bottle_angel_state.drops_in_map {
        let player = <&mut components::Player>::query()
            .get_mut(world, player.0)
            .unwrap();
        player.collected_hours += *hours_to_award as u32;
        *hours_to_award = 0;
        game_manager.request_return_to_heaven();
    }
}
