use crate::colliders;
use crate::components;
use crate::sprites;
use crate::svg_loader;

use rand::seq::SliceRandom;

pub fn create_network_switches(world: &mut legion::World, level: &svg_loader::SvgLevel) {
    let spawn_locations = level
        .spawnpoints
        .get("switch")
        .expect("no network switch spawn in this map");
    if spawn_locations.len() < 4 {
        panic!("too few network switch spawns");
    }
    for droppoint in spawn_locations.choose_multiple(&mut rand::thread_rng(), 4) {
        world.push((
            components::NetworkSwitch,
            components::Position::new(droppoint.x, droppoint.y),
            colliders::Collider::new_sensor_circle(70.0),
            components::Sprite::new(sprites::Sprite::NetworkSwitchDisconnected),
        ));
    }
}
