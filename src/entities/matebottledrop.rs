use crate::colliders;
use crate::components;
use crate::sprites;
use crate::svg_loader;

use rand::seq::SliceRandom;

pub fn create_drop_points(world: &mut legion::World, level: &svg_loader::SvgLevel) {
    let spawn_locations = level
        .spawnpoints
        .get("bottledrop")
        .expect("no mate bottle drops spawn in this map");
    if spawn_locations.len() < 4 {
        panic!("to few bottledrops");
    }
    for droppoint in spawn_locations.choose_multiple(&mut rand::thread_rng(), 4) {
        world.push((
            components::Matebottledrop,
            components::Position::new(droppoint.x, droppoint.y),
            colliders::Collider::new_sensor_circle(50.0),
            components::Sprite::new(sprites::Sprite::BottleDropPointFull),
        ));
    }
}
