use crate::colliders;
use crate::components;
use crate::resources;

use ncollide2d::shape;

const DEBUG_COLLIDER_COLOR: resources::Color = resources::Color {
    red: 1.0,
    green: 0.0,
    blue: 0.0,
    alpha: 0.5,
};

#[legion::system(for_each)]
pub fn draw_debug_colliders(
    position: &components::Position,
    collider: &colliders::Collider,
    rotation: Option<&components::Rotation>,
    #[resource] rendering: &mut resources::Rendering,
) {
    rendering.set_fill_style(&DEBUG_COLLIDER_COLOR);
    if let Some(ball) = collider.shape.downcast_ref::<shape::Ball<f32>>() {
        rendering.begin_path();
        rendering.arc(
            position.0.x as f64,
            position.0.y as f64,
            ball.radius as f64,
            0.0,
            std::f64::consts::TAU,
        );
        rendering.fill();
    } else if let Some(cuboid) = collider.shape.downcast_ref::<shape::Cuboid<f32>>() {
        if let Some(rotation) = rotation {
            let isometry = nalgebra::Isometry2::new(position.0.coords, rotation.0);
            let p1 =
                isometry * nalgebra::Point2::new(-cuboid.half_extents.x, -cuboid.half_extents.y);
            let p2 =
                isometry * nalgebra::Point2::new(-cuboid.half_extents.x, cuboid.half_extents.y);
            let p3 = isometry * nalgebra::Point2::new(cuboid.half_extents.x, cuboid.half_extents.y);
            let p4 =
                isometry * nalgebra::Point2::new(cuboid.half_extents.x, -cuboid.half_extents.y);

            rendering.begin_path();
            rendering.move_to(p1.x as f64, p1.y as f64);
            rendering.line_to(p2.x as f64, p2.y as f64);
            rendering.line_to(p3.x as f64, p3.y as f64);
            rendering.line_to(p4.x as f64, p4.y as f64);
            rendering.close_path();
            rendering.fill();
        } else {
            rendering.fill_rect(
                position.0.x as f64 - cuboid.half_extents.x as f64,
                position.0.y as f64 - cuboid.half_extents.y as f64,
                cuboid.half_extents.x as f64 * 2.0,
                cuboid.half_extents.y as f64 * 2.0,
            );
        }
    }
}
