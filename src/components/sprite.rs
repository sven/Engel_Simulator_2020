use crate::resources;

pub struct Sprite {
    pub image_handle: resources::ImageHandle,
    pub offset: nalgebra::Vector2<f32>,
}

impl Sprite {
    pub fn new<S: Into<resources::ImageHandle>>(handle: S) -> Self {
        Self::with_offset(handle, nalgebra::Vector2::new(0.0, 0.0))
    }

    pub fn with_offset<S: Into<resources::ImageHandle>>(
        handle: S,
        offset: nalgebra::Vector2<f32>,
    ) -> Self {
        Sprite {
            image_handle: handle.into(),
            offset,
        }
    }
}
