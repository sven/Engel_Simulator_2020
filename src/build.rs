//! Get git version to be displayed in-game
use std::fs;
use std::path;
use std::process;

use std::io::Read;

fn get_version_string() -> String {
    let output = process::Command::new("git")
        .args(&["describe", "--always", "--dirty", "--long"])
        .current_dir(env!("CARGO_MANIFEST_DIR"))
        .output()
        .unwrap();

    assert!(output.status.success());
    String::from_utf8_lossy(&output.stdout).to_string()
}

fn main() {
    println!("cargo:rustc-env=VERSION_INFO={}", get_version_string());

    let gitdir = path::PathBuf::from(".git");
    if !fs::metadata(&gitdir).map(|m| m.is_dir()).unwrap_or(false) {
        println!("cargo:warning=`.git` is not a directory! Version info might be incorrect.");
        return;
    }
    println!("cargo:rerun-if-changed={}", gitdir.join("HEAD").display());

    // Parse the ref from .git/HEAD and add it, too
    let mut f = fs::File::open(gitdir.join("HEAD")).unwrap();
    let mut content = String::new();
    f.read_to_string(&mut content).unwrap();
    if let Some(refname) = content.split(": ").nth(1).map(|s| s.trim_end_matches("\n")) {
        let refpath = gitdir.join(refname);
        if !fs::metadata(&refpath).map(|m| m.is_file()).unwrap_or(false) {
            println!("cargo:warning=git ref {:?} does not seem to exist", refname);
            return;
        }
        println!("cargo:rerun-if-changed={}", refpath.display());
    }
}
