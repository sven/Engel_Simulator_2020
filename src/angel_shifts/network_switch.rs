use crate::colliders;
use crate::components;
use crate::entities;
use crate::resources;
use crate::sprites;
use crate::svg_loader;
use crate::utils;

pub struct NetworkSwitchShift {
    hours: usize,
    level: String,
}

impl NetworkSwitchShift {
    pub fn generate(rng: &mut impl rand::Rng) -> NetworkSwitchShift {
        use rand::seq::SliceRandom;

        let compatible_levels = ["assembly-hall-1.svg"];

        NetworkSwitchShift {
            hours: rng.gen_range(1, 3),
            level: compatible_levels.choose(rng).unwrap().to_string(),
        }
    }
}

pub struct NetworkSwitchAngelState {
    reconnected_switches: usize,
    switches_in_map: usize,
    stats_element: web_sys::Element,
}

impl NetworkSwitchAngelState {
    fn new(switches_in_map: usize, stats_element: web_sys::Element) -> NetworkSwitchAngelState {
        NetworkSwitchAngelState {
            reconnected_switches: 0,
            switches_in_map,
            stats_element,
        }
    }

    fn update_stats(&self) {
        self.stats_element.set_text_content(Some(&format!(
            "{}/{}",
            self.reconnected_switches, self.switches_in_map
        )));
    }
}

impl super::AngelShiftImpl for NetworkSwitchShift {
    fn metadata(&self) -> super::ShiftMetadata {
        super::ShiftMetadata {
            title: "Fix the internet".to_owned(),
            description: "Check what's wrong with the network switches.".to_owned(),
            hours: self.hours,
        }
    }

    fn level_name(&self) -> &str {
        &self.level
    }

    fn init_gameworld(
        &self,
        world: &mut legion::World,
        resources: &mut legion::Resources,
        schedule_builder: &mut legion::systems::Builder,
        level: &svg_loader::SvgLevel,
    ) {
        // Display objective
        let objective = utils::get_element_by_id::<web_sys::Element>("ingame-objective").unwrap();
        objective.set_inner_html(
            r#"
            <text x="30" y="0" class="stats-label">Switches online:</text>
            <text id="ingame-network-stats" x="430" y="0" class="stats-number">0/4</text>
        "#,
        );
        let stats = utils::get_element_by_id::<web_sys::Element>("ingame-network-stats").unwrap();
        entities::create_network_switches(world, level);
        resources.insert(NetworkSwitchAngelState::new(4, stats));

        schedule_builder
            .add_thread_local(reconnect_switches_system())
            .add_thread_local(update_network_switch_shift_system(self.hours));
    }
}

#[legion::system]
#[read_component(colliders::Collider)]
#[read_component(components::NetworkSwitch)]
#[write_component(components::Sprite)]
pub fn reconnect_switches(
    world: &mut legion::world::SubWorld,
    cmd: &mut legion::systems::CommandBuffer,
    #[resource] player: &resources::Player,
    #[resource] collision_world: &colliders::CollisionWorld,
    #[resource] angel_state: &mut NetworkSwitchAngelState,
) {
    use legion::IntoQuery;

    let collider = <&colliders::Collider>::query()
        .get(world, player.0)
        .unwrap();

    let mut switches = <(&components::NetworkSwitch, &mut components::Sprite)>::query();
    for pair in collision_world
        .world
        .proximities_with(collider.handle.unwrap(), false)
        .unwrap()
    {
        if pair.3 != ncollide2d::query::Proximity::Intersecting {
            continue;
        }

        let entity = *collision_world.world.objects.get(pair.1).unwrap().data();
        if let Ok((_, sprite)) = switches.get_mut(world, entity) {
            *sprite = components::Sprite::new(sprites::Sprite::NetworkSwitchConnected);
            cmd.remove_component::<components::NetworkSwitch>(entity);
            angel_state.reconnected_switches += 1;
            angel_state.update_stats();
        }
    }
}

#[legion::system]
#[write_component(components::Player)]
pub fn update_network_switch_shift(
    #[state] hours_to_award: &mut usize,
    world: &mut legion::world::SubWorld,
    #[resource] player: &mut resources::Player,
    #[resource] angel_state: &mut NetworkSwitchAngelState,
    #[resource] game_manager: &mut resources::GameManager,
) {
    use legion::IntoQuery;

    if angel_state.reconnected_switches >= angel_state.switches_in_map {
        let player = <&mut components::Player>::query()
            .get_mut(world, player.0)
            .unwrap();
        player.collected_hours += *hours_to_award as u32;
        *hours_to_award = 0;
        game_manager.request_return_to_heaven();
    }
}
