pub mod bottle_angel;
mod definitions;
pub mod network_switch;

pub use definitions::AngelShift;
pub use definitions::AngelShiftImpl;
pub use definitions::ShiftMetadata;

pub fn generate_random_shift(rng: &mut impl rand::Rng) -> AngelShift {
    AngelShift(match rng.gen_range(0usize, 2) {
        0 => Box::new(bottle_angel::BottleAngelShift::generate(rng)),
        1 => Box::new(network_switch::NetworkSwitchShift::generate(rng)),
        _ => unreachable!(),
    })
}
