use crate::components;
use legion::IntoQuery;
use ncollide2d::pipeline;
use ncollide2d::shape;
use std::sync::mpsc;

pub const COLLIDER_STATIC: usize = 0;
pub const COLLIDER_PLAYER: usize = 1;
pub const COLLIDER_SENSOR: usize = 2;

pub struct Collider {
    pub handle: Option<ncollide2d::pipeline::CollisionObjectSlabHandle>,
    drop_notifier: Option<mpsc::SyncSender<ncollide2d::pipeline::CollisionObjectSlabHandle>>,
    pub shape: shape::ShapeHandle<f32>,
    groups: pipeline::CollisionGroups,
    query_type: pipeline::GeometricQueryType<f32>,
}

impl Collider {
    pub fn new(
        shape: shape::ShapeHandle<f32>,
        groups: pipeline::CollisionGroups,
        proximity_only: bool,
    ) -> Collider {
        let query_type = if proximity_only {
            pipeline::GeometricQueryType::Proximity(0.0)
        } else {
            pipeline::GeometricQueryType::Contacts(0.0, 0.0)
        };

        Collider {
            handle: None,
            drop_notifier: None,
            shape,
            groups,
            query_type,
        }
    }

    pub fn new_player(radius: f32) -> Collider {
        let shape = shape::Ball::new(radius);
        let groups = pipeline::CollisionGroups::new()
            .with_membership(&[COLLIDER_PLAYER])
            .with_whitelist(&[COLLIDER_STATIC, COLLIDER_SENSOR]);

        Self::new(shape::ShapeHandle::new(shape), groups, false)
    }

    pub fn new_sensor_circle(radius: f32) -> Collider {
        let shape = shape::Ball::new(radius);
        let groups = pipeline::CollisionGroups::new()
            .with_membership(&[COLLIDER_SENSOR])
            .with_whitelist(&[COLLIDER_PLAYER])
            .with_blacklist(&[COLLIDER_STATIC]);

        Self::new(shape::ShapeHandle::new(shape), groups, true)
    }

    pub fn new_static_circle(radius: f32) -> Collider {
        let shape = shape::Ball::new(radius);
        let groups = pipeline::CollisionGroups::new()
            .with_membership(&[COLLIDER_STATIC])
            .with_blacklist(&[COLLIDER_STATIC]);

        Self::new(shape::ShapeHandle::new(shape), groups, false)
    }

    pub fn new_static_rect(width: f32, height: f32) -> Collider {
        let shape = shape::Cuboid::new(nalgebra::Vector2::new(width / 2.0, height / 2.0));
        let groups = pipeline::CollisionGroups::new()
            .with_membership(&[COLLIDER_STATIC])
            .with_blacklist(&[COLLIDER_STATIC]);

        Self::new(shape::ShapeHandle::new(shape), groups, false)
    }
}

impl Drop for Collider {
    fn drop(&mut self) {
        if let (Some(drop_notifier), Some(handle)) = (self.drop_notifier.take(), self.handle.take())
        {
            if let Err(e) = drop_notifier.try_send(handle) {
                crate::console_warn!("Failed to schedule collider drop!! Error: {}", e);
            }
        }
    }
}

pub struct CollisionWorld {
    pub world: ncollide2d::world::CollisionWorld<f32, legion::Entity>,
    drop_notifier: mpsc::SyncSender<ncollide2d::pipeline::CollisionObjectSlabHandle>,
    drop_receiver: mpsc::Receiver<ncollide2d::pipeline::CollisionObjectSlabHandle>,
}

impl CollisionWorld {
    pub fn new() -> CollisionWorld {
        let (drop_notifier, drop_receiver) = mpsc::sync_channel(32);
        CollisionWorld {
            world: ncollide2d::world::CollisionWorld::new(0.5),
            drop_notifier,
            drop_receiver,
        }
    }

    pub fn interferences_with_point(&self, point: &nalgebra::Point2<f32>) -> Vec<legion::Entity> {
        self.world
            .interferences_with_point(point, &pipeline::CollisionGroups::new())
            .map(|(_, collision_object)| *collision_object.data())
            .collect()
    }
}

#[legion::system]
#[write_component(Collider)]
#[read_component(components::Position)]
pub fn synchronize_collisision_world(
    world: &mut legion::world::SubWorld,
    #[resource] collision_world: &mut CollisionWorld,
) {
    let mut query = <(legion::Entity, &mut Collider, &components::Position)>::query();
    for (ent, collider, position) in query.iter_mut(world) {
        // If this collider was not yet added into the collision world, take care of that now.
        if collider.handle.is_none() {
            let (handle, _) = collision_world.world.add(
                nalgebra::Isometry2::new(position.0.coords, 0.0),
                collider.shape.clone(),
                collider.groups,
                collider.query_type,
                *ent,
            );
            collider.handle = Some(handle);
            collider.drop_notifier = Some(collision_world.drop_notifier.clone());
        }
    }

    while let Ok(handle) = collision_world.drop_receiver.try_recv() {
        collision_world.world.remove(&[handle]);
    }
}

#[legion::system]
#[read_component(Collider)]
#[read_component(components::Position)]
#[read_component(components::Rotation)]
pub fn update_collision_world(
    world: &legion::world::SubWorld,
    #[resource] collision_world: &mut CollisionWorld,
) {
    // Update all entity positions
    let mut query = <(
        &Collider,
        &components::Position,
        Option<&components::Rotation>,
    )>::query()
    .filter(
        legion::maybe_changed::<components::Position>()
            | legion::maybe_changed::<components::Rotation>(),
    );
    for (collider, position, rotation_opt) in query.iter(world) {
        let handle = collider
            .handle
            .expect("synchronize_collisision_world() did not run yet!");
        let collision_object = collision_world
            .world
            .get_mut(handle)
            .expect("missing collider");

        collision_object.set_position(nalgebra::Isometry2::new(
            position.0.coords,
            rotation_opt.map(|r| r.0).unwrap_or(0.0),
        ));
    }

    collision_world.world.update();
}
