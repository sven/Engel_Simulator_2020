use crate::components;
use crate::resources;

use legion::IntoQuery;

#[legion::system(for_each)]
#[read_component(components::Player)]
pub fn sanity_goes_up_and_down(
    player: &mut components::Player,
    #[resource] clock: &resources::Clock,
) {
    player.sanity = (clock.wall_time() as f32).sin() * 0.5 + 0.5;
}

#[legion::system]
#[read_component(components::Player)]
pub fn update_sanity_bar(
    world: &legion::world::SubWorld,
    #[resource] player: &resources::Player,
    #[state] sanity_bar: &mut web_sys::Element,
) {
    let mut players = <&components::Player>::query();
    let player = players.get(world, player.0).unwrap();

    sanity_bar
        .set_attribute("width", &(player.sanity * 1000.0).to_string())
        .unwrap();
}

#[legion::system]
#[write_component(components::Player)]
pub fn player_sanity_check(
    world: &mut legion::world::SubWorld,
    #[resource] player: &resources::Player,
    #[resource] game_manager: &mut resources::GameManager,
) {
    let mut players = <&mut components::Player>::query();
    let player = players.get_mut(world, player.0).unwrap();

    if player.sanity <= 0.0 {
        game_manager.request_game_over();
    }
}
