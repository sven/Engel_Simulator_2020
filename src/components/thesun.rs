use crate::colors;
use crate::components;
use crate::resources;

pub struct TheSun;

#[legion::system(for_each)]
pub fn draw_thesun(
    _: &TheSun,
    pos: &components::Position,
    #[resource] rendering: &mut resources::Rendering,
    #[resource] clock: &resources::Clock,
) {
    // TODO: Replace by proper API
    rendering.begin_path();
    rendering.set_fill_style(&colors::PRIMARY1_SHADE1);
    rendering.arc(
        pos.0.x as f64,
        pos.0.y as f64,
        30.0,
        0.0,
        std::f64::consts::TAU,
    );
    rendering.fill();

    let now = clock.wall_time();
    rendering.set_line_width(10.0);

    rendering.begin_path();
    rendering.set_stroke_style(&colors::PRIMARY1_SHADE2);
    rendering.arc(
        pos.0.x as f64,
        pos.0.y as f64,
        40.0,
        0.0 + now,
        0.4 * std::f64::consts::TAU + now,
    );
    rendering.stroke();

    rendering.begin_path();
    rendering.set_stroke_style(&colors::PRIMARY1_SHADE3);
    rendering.arc(
        pos.0.x as f64,
        pos.0.y as f64,
        55.0,
        4.0 - now,
        4.0 + 0.66 * std::f64::consts::TAU - now,
    );
    rendering.stroke();
}
