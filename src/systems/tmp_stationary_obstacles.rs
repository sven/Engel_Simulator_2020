use crate::colliders;
use crate::colors;
use crate::components;
use crate::resources;

#[legion::system(for_each)]
pub fn draw_tmp_stationary_obstacles_barrier(
    obstacle: &components::ObstacleBarrier,
    pos: &components::Position,
    #[resource] rendering: &mut resources::Rendering,
) {
    rendering.begin_path();
    rendering.set_fill_style(&colors::PRIMARY3_SHADE3);
    let w = obstacle.width() as f64;
    let h = obstacle.height() as f64;

    rendering.fill_rect(pos.0.x as f64 - w / 2.0, pos.0.y as f64 - h / 2.0, w, h);
    rendering.fill();
}

#[legion::system]
#[read_component(colliders::Collider)]
#[read_component(components::ObstacleInsanity)]
#[write_component(components::Player)]
pub fn reduce_sanity_obstacle(
    world: &mut legion::world::SubWorld,
    #[resource] player: &mut resources::Player,
    #[resource] clock: &resources::Clock,
    #[resource] collision_world: &colliders::CollisionWorld,
) {
    use legion::IntoQuery;
    let collider = <&colliders::Collider>::query()
        .get(world, player.0)
        .unwrap();
    let mut obs_insanity = <&components::ObstacleInsanity>::query();
    for pair in collision_world
        .world
        .proximities_with(collider.handle.unwrap(), false)
        .unwrap()
    {
        if pair.3 != ncollide2d::query::Proximity::Intersecting {
            continue;
        }
        let entity = *collision_world.world.objects.get(pair.1).unwrap().data();
        if obs_insanity.get(world, entity).is_ok() {
            let player = <&mut components::Player>::query()
                .get_mut(world, player.0)
                .unwrap();
            player.sanity -= 0.15 * clock.frame_delta();
        }
    }
}
