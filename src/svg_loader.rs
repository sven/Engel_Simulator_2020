use std::collections;
use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;

use crate::colliders;
use crate::components;
use crate::utils;

// rustfmt skip because of a bug where rustfmt removes the `async` keyword ...
#[rustfmt::skip]
#[wasm_bindgen(raw_module = "./svg-level-loader")]
extern "C" {
    async fn load_svg(url: &str) -> JsValue;
    fn clean_svg(loading_id: &str);
    fn hide_canvas_node_by_query(loading_id: &str, query: &str);
    fn show_canvas_node_by_query(loading_id: &str, query: &str);
    async fn convert_svg_to_image(loading_id: &str) -> JsValue;
}

pub struct SvgLoader {
    loading_id: String,
}

impl SvgLoader {
    pub async fn load_svg(url: &str) -> Self {
        let loading_id = load_svg(url).await.as_string().unwrap();
        SvgLoader { loading_id }
    }

    pub fn hide_canvas_node_by_query(&self, query: &str) {
        hide_canvas_node_by_query(&self.loading_id, query)
    }
    pub fn show_canvas_node_by_query(&self, query: &str) {
        show_canvas_node_by_query(&self.loading_id, query)
    }

    pub async fn convert_svg_to_image(&self) -> web_sys::HtmlImageElement {
        convert_svg_to_image(&self.loading_id)
            .await
            .dyn_into()
            .unwrap()
    }

    pub fn query_selector_all(&self, query: &str) -> web_sys::NodeList {
        web_sys::window()
            .unwrap()
            .document()
            .unwrap()
            .get_element_by_id(&self.loading_id)
            .unwrap()
            .query_selector_all(query)
            .unwrap()
    }

    pub fn verify_viewbox(&self) -> bool {
        let svg = utils::get_element_by_id::<web_sys::Element>(&self.loading_id)
            .unwrap()
            .query_selector("svg")
            .unwrap()
            .unwrap();
        let svg_width = svg.get_attribute("width").unwrap();
        let svg_height = svg.get_attribute("height").unwrap();
        let viewbox = svg.get_attribute("viewBox").unwrap();
        let expected_viewbox = format!("0 0 {} {}", svg_width, svg_height);
        viewbox == expected_viewbox
    }
}

impl Drop for SvgLoader {
    fn drop(&mut self) {
        clean_svg(&self.loading_id)
    }
}

pub async fn load_svg_as_image(url: &str) -> web_sys::HtmlImageElement {
    SvgLoader::load_svg(url).await.convert_svg_to_image().await
}

#[derive(Debug)]
pub enum Collider {
    Circle {
        cx: f32,
        cy: f32,
        radius: f32,
    },
    Rect {
        x: f32,
        y: f32,
        width: f32,
        height: f32,
        rotation: Option<f32>,
    },
}

impl Collider {
    fn load_from_svg(svg_loader: &SvgLoader) -> Vec<Self> {
        let mut colliders = Vec::new();
        let nodes = svg_loader.query_selector_all(
            "g[inkscape\\:groupmode=\"layer\"][inkscape\\:label=\"collider\"] circle",
        );
        colliders.append(
            &mut (0..nodes.length())
                .map(|i| {
                    let node = nodes.get(i).unwrap();
                    let element: web_sys::Element = node.dyn_into().unwrap();
                    let cx: f32 = element.get_attribute("cx").unwrap().parse().unwrap();
                    let cy: f32 = element.get_attribute("cy").unwrap().parse().unwrap();
                    let radius: f32 = element.get_attribute("r").unwrap().parse().unwrap();
                    element.remove();
                    Collider::Circle { cx, cy, radius }
                })
                .collect(),
        );

        let nodes = svg_loader.query_selector_all(
            "g[inkscape\\:groupmode=\"layer\"][inkscape\\:label=\"collider\"] rect",
        );
        colliders.append(
            &mut (0..nodes.length())
                .map(|i| {
                    let node = nodes.get(i).unwrap();
                    let element: web_sys::Element = node.dyn_into().unwrap();
                    let x: f32 = element.get_attribute("x").unwrap().parse().unwrap();
                    let y: f32 = element.get_attribute("y").unwrap().parse().unwrap();
                    let width: f32 = element.get_attribute("width").unwrap().parse().unwrap();
                    let height: f32 = element.get_attribute("height").unwrap().parse().unwrap();
                    let rotation = element.get_attribute("transform").map(|t| {
                        t.strip_prefix("rotate(")
                            .expect("`transform` does not seem to be a rotation")
                            .strip_suffix(")")
                            .expect("malformed `transform`")
                            .parse()
                            .expect("`transform` is not a number")
                    });
                    element.remove();
                    Collider::Rect {
                        x,
                        y,
                        width,
                        height,
                        rotation,
                    }
                })
                .collect(),
        );

        let nodes = svg_loader.query_selector_all(
            "g[inkscape\\:groupmode=\"layer\"][inkscape\\:label=\"collider\"] rect",
        );
        if nodes.length() > 0 {
            panic!("Unknown elements in collider layer! Only <circle /> and <rect /> are allowed right now.");
        }

        colliders
    }
}

#[derive(Debug)]
pub struct SvgLevel {
    pub foreground_image: web_sys::HtmlImageElement,
    pub background_image: web_sys::HtmlImageElement,
    pub colliders: Vec<Collider>,
    pub spawnpoints: collections::HashMap<String, Vec<nalgebra::Point2<f32>>>,
}

impl SvgLevel {
    pub async fn load_from_svg_file(url: &str) -> Result<Self, JsValue> {
        let svg_loader = SvgLoader::load_svg(url).await;

        if !svg_loader.verify_viewbox() {
            return Err(JsValue::from_str("SVG Viewbox and SVG size don't match"));
        }

        svg_loader.hide_canvas_node_by_query("g[inkscape\\:groupmode=\"layer\"]");

        svg_loader.show_canvas_node_by_query(
            "g[inkscape\\:groupmode=\"layer\"][inkscape\\:label=\"background\"]",
        );
        let background_image = svg_loader.convert_svg_to_image().await;
        svg_loader.hide_canvas_node_by_query(
            "g[inkscape\\:groupmode=\"layer\"][inkscape\\:label=\"background\"]",
        );

        svg_loader.show_canvas_node_by_query(
            "g[inkscape\\:groupmode=\"layer\"][inkscape\\:label=\"foreground\"]",
        );
        let foreground_image = svg_loader.convert_svg_to_image().await;
        svg_loader.hide_canvas_node_by_query(
            "g[inkscape\\:groupmode=\"layer\"][inkscape\\:label=\"foreground\"]",
        );

        let colliders = Collider::load_from_svg(&svg_loader);

        let mut spawnpoints = collections::HashMap::new();
        let nodes = svg_loader
            .query_selector_all(r#"g[inkscape\:groupmode="layer"][inkscape\:label="spawns"] text"#);
        for node in (0..nodes.length()).map(|i| nodes.get(i).unwrap()) {
            let element: &web_sys::Element = node.dyn_ref().unwrap();
            let x: f32 = element.get_attribute("x").unwrap().parse().unwrap();
            let y: f32 = element.get_attribute("y").unwrap().parse().unwrap();
            let pos = nalgebra::Point2::new(x, y);

            let tspan: web_sys::Element = node
                .first_child()
                .expect("expected <tspan /> in <text />")
                .dyn_into()
                .unwrap();
            let spawntype = tspan.inner_html();

            spawnpoints
                .entry(spawntype)
                .or_insert_with(Vec::new)
                .push(pos);
        }

        let svg_level = SvgLevel {
            foreground_image,
            background_image,
            colliders,
            spawnpoints,
        };
        Ok(svg_level)
    }

    pub fn apply_colliders(&self, world: &mut legion::World) {
        for collider in &self.colliders {
            match collider {
                Collider::Circle { cx, cy, radius } => {
                    let collider = colliders::Collider::new_static_circle(*radius);
                    let position = components::Position::new(*cx, *cy);
                    world.push((collider, position));
                }
                Collider::Rect {
                    x,
                    y,
                    width,
                    height,
                    rotation,
                } => {
                    let collider = colliders::Collider::new_static_rect(*width, *height);
                    if let Some(svg_rotation) = rotation {
                        // The rotation in SVG is not around the center of the object, but around
                        // the origin [0:0].  We'll have to recalculate the position here to
                        // match out internal format instead.

                        // Convert to radians first
                        let angle = svg_rotation.to_radians();
                        let offset = nalgebra::Point2::new(x + width / 2.0, y + height / 2.0);
                        let pos_rotated = nalgebra::Rotation2::new(angle) * offset;

                        let position = components::Position(pos_rotated);
                        let rotation = components::Rotation::new(angle);
                        world.push((collider, position, rotation));
                    } else {
                        // Position must be the center of the rectangle
                        let position = components::Position::new(x + width / 2.0, y + height / 2.0);
                        world.push((collider, position));
                    }
                }
            }
        }
    }

    pub fn width(&self) -> u32 {
        self.foreground_image.width()
    }

    pub fn height(&self) -> u32 {
        self.foreground_image.height()
    }
}
