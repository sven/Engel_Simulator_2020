use crate::components;
use crate::resources;

#[legion::system(for_each)]
#[read_component(components::Position)]
#[read_component(components::Sprite)]
pub fn draw_sprites(
    #[resource] rendering: &resources::Rendering,
    position: &components::Position,
    sprite: &components::Sprite,
) {
    let spritesize = rendering.get_image_size(&sprite.image_handle).unwrap();
    let upper_left = position.0 - spritesize / 2.0 + sprite.offset;
    rendering
        .draw_image(
            &sprite.image_handle,
            upper_left.x as f64,
            upper_left.y as f64,
        )
        .unwrap();
}
