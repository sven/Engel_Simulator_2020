pub struct Position(pub nalgebra::Point2<f32>);

impl Position {
    pub fn new(x: f32, y: f32) -> Position {
        Position(nalgebra::Point2::new(x, y))
    }
}

pub struct Rotation(pub f32);

impl Rotation {
    // angle in radians
    pub fn new(angle: f32) -> Rotation {
        Rotation(angle)
    }
}
