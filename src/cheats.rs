use crate::gamestate;

use anyhow::Context;

use wasm_bindgen::prelude::*;

static mut SMHANDLE: Option<gamestate::StateMachineHandle> = None;
static mut CHEAT_PASSWORD: Option<String> = None;

pub fn initialize_cheats(handle: gamestate::StateMachineHandle) {
    // SAFETY: We know that concurrency is impossible because Javascript is (in this case)
    // single-threaded.
    unsafe {
        SMHANDLE = Some(handle);
    }
}

#[non_exhaustive]
#[derive(Debug, Clone)]
pub enum CheatCommand {
    SetSanity(f32),
    SetShifts(u32),
    GetPlayer(),
}

fn get_cheat_state() -> anyhow::Result<&'static gamestate::StateMachineHandle> {
    // SAFETY: We know that concurrency is impossible because Javascript is (in this case)
    // single-threaded.
    unsafe {
        if CHEAT_PASSWORD.as_deref() != Some(option_env!("ENGEL_CHEAT_CODE").unwrap_or("uhagre7")) {
            anyhow::bail!("you shall not cheat!");
        }

        SMHANDLE.as_ref().context("cheats not initialized")
    }
}

#[wasm_bindgen]
pub fn cheat_enable(password: &str) {
    // SAFETY: We know that concurrency is impossible because Javascript is (in this case)
    // single-threaded.
    unsafe {
        CHEAT_PASSWORD = Some(
            password
                .chars()
                .map(|c| match c {
                    'A'...'M' | 'a'...'m' => ((c as u8) + 13) as char,
                    'N'...'Z' | 'n'...'z' => ((c as u8) - 13) as char,
                    '0'...'9' => (0x39 - (c as u8) + 0x30) as char,
                    _ => c,
                })
                .collect(),
        );
    }
}

#[wasm_bindgen]
pub fn cheat_set_sanity(val: f32) -> Result<(), wasm_bindgen::JsValue> {
    match || -> anyhow::Result<()> {
        // this is nice
        let state = get_cheat_state()?;
        state.do_cheat(CheatCommand::SetSanity(val));
        Ok(())
    }() {
        // this is not nice
        Ok(()) => Ok(()),
        Err(err) => Err(wasm_bindgen::JsValue::from_str(&format!("{}", err))),
    }
}

#[wasm_bindgen]
pub fn cheat_set_shifts(val: u32) -> Result<(), wasm_bindgen::JsValue> {
    match || -> anyhow::Result<()> {
        let state = get_cheat_state()?;
        state.do_cheat(CheatCommand::SetShifts(val));
        Ok(())
    }() {
        // this is not nice
        Ok(()) => Ok(()),
        Err(err) => Err(wasm_bindgen::JsValue::from_str(&format!("{}", err))),
    }
}
#[wasm_bindgen]
pub fn cheat_get_player() -> Result<(), wasm_bindgen::JsValue> {
    match || -> anyhow::Result<()> {
        let state = get_cheat_state()?;
        state.do_cheat(CheatCommand::GetPlayer());
        Ok(())
    }() {
        // this is not nice
        Ok(()) => Ok(()),
        Err(err) => Err(wasm_bindgen::JsValue::from_str(&format!("{}", err))),
    }
}
