use crate::colors;
use crate::components;
use crate::resources;
use legion::IntoQuery;

pub struct Edge {
    node1: legion::Entity,
    node2: legion::Entity,
}

impl Edge {
    pub fn new(node1: legion::Entity, node2: legion::Entity) -> Edge {
        Edge { node1, node2 }
    }
}

#[legion::system]
#[read_component(Edge)]
#[read_component(components::Position)]
pub fn draw_edges(
    world: &legion::world::SubWorld,
    #[resource] rendering: &mut resources::Rendering,
) {
    // TODO: Replace by proper API
    for edge in <&Edge>::query().iter(world) {
        let mut positions = <&components::Position>::query();
        let pos1 = positions.get(world, edge.node1).unwrap();
        let pos2 = positions.get(world, edge.node2).unwrap();

        rendering.begin_path();
        rendering.set_line_width(7.0);
        rendering.set_stroke_style(&colors::PRIMARY2_SHADE3);
        rendering.move_to(pos1.0.x as f64, pos1.0.y as f64);
        rendering.line_to(pos2.0.x as f64, pos2.0.y as f64);
        rendering.stroke();
    }
}
