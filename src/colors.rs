#![allow(unused_attributes)]
#![rustfmt::skip]
use crate::resources::Color;

pub const BACKGROUND: Color      = Color { red: 0.063, green: 0.055, blue: 0.137, alpha: 1.0};

pub const PRIMARY1_SHADE1: Color = Color { red: 0.698, green: 0.224, blue: 1.000, alpha: 1.0};
pub const PRIMARY1_SHADE2: Color = Color { red: 0.404, green: 0.008, blue: 0.624, alpha: 1.0};
pub const PRIMARY1_SHADE3: Color = Color { red: 0.267, green: 0.000, blue: 0.412, alpha: 1.0};
pub const PRIMARY1_SHADE4: Color = Color { red: 0.141, green: 0.000, blue: 0.220, alpha: 1.0};
pub const PRIMARY2_SHADE1: Color = Color { red: 0.408, green: 0.000, blue: 0.906, alpha: 1.0};
pub const PRIMARY2_SHADE2: Color = Color { red: 0.255, green: 0.000, blue: 0.545, alpha: 1.0};
pub const PRIMARY2_SHADE3: Color = Color { red: 0.165, green: 0.000, blue: 0.369, alpha: 1.0};
pub const PRIMARY2_SHADE4: Color = Color { red: 0.078, green: 0.000, blue: 0.184, alpha: 1.0};
pub const PRIMARY3_SHADE1: Color = Color { red: 0.020, green: 0.725, blue: 0.925, alpha: 1.0};
pub const PRIMARY3_SHADE2: Color = Color { red: 0.000, green: 0.463, blue: 0.663, alpha: 1.0};
pub const PRIMARY3_SHADE3: Color = Color { red: 0.008, green: 0.365, blue: 0.518, alpha: 1.0};
pub const PRIMARY3_SHADE4: Color = Color { red: 0.000, green: 0.165, blue: 0.227, alpha: 1.0};
