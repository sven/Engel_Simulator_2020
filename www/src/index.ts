// @ts-ignore
import * as wasm from "engel-simulator-2020";
import "./styles.scss";

wasm.start();

(window as any).cheats = Object.assign(
  {},
  ...Object.keys(wasm)
    .filter((f) => f.startsWith("cheat_"))
    .map((f) => ({[f.substr(6)]: (wasm as any)[f]}))
);
