use crate::angel_shifts;
use crate::colors;
use crate::components;
use crate::gamestate;
use crate::resources;
use crate::sprites;
use crate::states;
use crate::svg_loader;
use crate::utils;

pub struct LevelLoadingState {
    node_world: legion::World,
    resources: legion::Resources,
    node_schedule: legion::Schedule,

    player: components::Player,
    assigned_shift: Option<angel_shifts::AngelShift>,
}

impl LevelLoadingState {
    pub fn new(
        player: components::Player,
        assigned_shift: angel_shifts::AngelShift,
    ) -> LevelLoadingState {
        let mut resources = legion::Resources::default();
        resources.insert(resources::Clock::new());
        resources.insert(resources::Rendering::new("game-canvas").unwrap());

        let mut node_world = legion::World::default();
        node_world.push((
            components::TheSun,
            components::Position::new(1920.0 / 2.0, 1080.0 / 2.0),
        ));

        let node_schedule = legion::Schedule::builder()
            .add_thread_local(components::draw_thesun_system())
            .build();

        // Clean the objective display before the next shift
        let objective = utils::get_element_by_id::<web_sys::Element>("ingame-objective").unwrap();
        objective.set_text_content(None);

        LevelLoadingState {
            node_world,
            resources,
            node_schedule,
            player,
            assigned_shift: Some(assigned_shift),
        }
    }
}

impl gamestate::State for LevelLoadingState {
    fn init(&mut self, init: gamestate::StateInitializer) -> gamestate::Transition {
        let mut rendering = resources::Rendering::new("game-canvas").unwrap();
        let handle = init.get_handle();
        let player = self.player.clone();
        let assigned_shift = self.assigned_shift.take().unwrap();
        wasm_bindgen_futures::spawn_local(async move {
            let level_name = assigned_shift.level_name();
            let level = svg_loader::SvgLevel::load_from_svg_file(&format!(
                "resources/levels/{}",
                level_name
            ))
            .await
            .unwrap();

            sprites::Sprite::load_and_register_all(&mut rendering).await;

            handle.do_transition(gamestate::Transition::replace(states::InGameState::new(
                level,
                rendering,
                player.clone(),
                assigned_shift,
            )));
        });
        gamestate::Transition::Loop
    }

    fn update(&mut self, timestamp: f64) -> gamestate::Transition {
        self.resources
            .get_mut::<resources::Clock>()
            .unwrap()
            .update(timestamp);

        {
            let rendering = self.resources.get_mut::<resources::Rendering>().unwrap();
            rendering.set_fill_style(&colors::BACKGROUND);
            rendering.fill_rect(0.0, 0.0, 1920.0, 1080.0);
        }

        self.node_schedule
            .execute(&mut self.node_world, &mut self.resources);

        gamestate::Transition::Loop
    }
}
