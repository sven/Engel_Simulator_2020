use std::cell::RefCell;
use std::rc::Rc;

use crate::utils;
use wasm_bindgen::closure;

pub enum Transition {
    /// Schedule a call of [`State::update`] with `requestAnimationFrame`.
    Loop,
    /// Sleep and wait for any registered event-handler to fire.
    Sleep,
    /// Either `Loop` or `Sleep`, whatever happened last.
    Keep,
    /// Replace this state with a new one.
    Replace(Box<dyn State>),
    /// Push a new state on the stack.
    Push(Box<dyn State>),
    /// Pop this state from the stack and return to the last one.
    Pop,
}

impl std::fmt::Debug for Transition {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Transition::Loop => "Loop",
                Transition::Sleep => "Sleep",
                Transition::Keep => "Keep",
                Transition::Replace(_) => "Replace(..)",
                Transition::Push(_) => "Push(..)",
                Transition::Pop => "Pop",
            }
        )
    }
}

impl Transition {
    pub fn replace<S: State + 'static>(new: S) -> Transition {
        Transition::Replace(Box::new(new))
    }

    pub fn push<S: State + 'static>(new: S) -> Transition {
        Transition::Push(Box::new(new))
    }
}

pub trait State {
    fn init(&mut self, init: StateInitializer) -> Transition;
    fn deinit(&mut self) {}

    fn update(&mut self, _timestamp: f64) -> Transition {
        Transition::Keep
    }
    #[allow(unused_variables)]
    fn event(&mut self, event: Event) -> Transition {
        Transition::Keep
    }
}

#[non_exhaustive]
#[derive(Debug, Clone)]
pub enum Event<'a> {
    MouseClick {
        target: &'a str,
        coords: nalgebra::Point2<f32>,
    },
    KeyDown(&'a str),
    KeyUp(&'a str),
    Cheat(crate::cheats::CheatCommand),
}

pub type HandlerClosure = closure::Closure<dyn FnMut(wasm_bindgen::JsValue)>;
struct EventHandlers {
    pub mouse_handlers: std::collections::HashMap<String, HandlerClosure>,
    pub key_handlers: Option<(HandlerClosure, HandlerClosure)>,
}

impl EventHandlers {
    pub fn new() -> EventHandlers {
        EventHandlers {
            mouse_handlers: std::collections::HashMap::new(),
            key_handlers: None,
        }
    }

    pub fn register_onclick<F: FnMut(&web_sys::MouseEvent) + 'static>(
        &mut self,
        id: &str,
        mut f: F,
    ) {
        use wasm_bindgen::JsCast;

        let f = closure::Closure::wrap(Box::new(move |event: wasm_bindgen::JsValue| {
            let event = event
                .dyn_ref::<web_sys::MouseEvent>()
                .expect("no mouse event?");

            f(event)
        }) as Box<dyn FnMut(wasm_bindgen::JsValue)>);

        if let Ok(el) = utils::get_element_by_id::<web_sys::SvgElement>(id) {
            el.set_onclick(Some(f.as_ref().unchecked_ref()));
        } else if let Ok(el) = utils::get_element_by_id::<web_sys::HtmlElement>(id) {
            el.set_onclick(Some(f.as_ref().unchecked_ref()));
        } else {
            panic!("Cannot find appropriate element {:?}", id);
        }

        self.mouse_handlers.insert(id.to_owned(), f);
    }

    pub fn register_keyevents<F: FnMut(bool, &web_sys::KeyboardEvent) + 'static>(&mut self, f: F) {
        use wasm_bindgen::JsCast;

        let f = Rc::new(RefCell::new(f));
        let f2 = f.clone();
        let fdown = closure::Closure::wrap(Box::new(move |event: wasm_bindgen::JsValue| {
            let event = event
                .dyn_ref::<web_sys::KeyboardEvent>()
                .expect("no keyboard event?");

            // Drop keyDown events which are repating
            if event.repeat() {
                return;
            }

            f.borrow_mut()(true, event)
        }) as Box<dyn FnMut(wasm_bindgen::JsValue)>);
        let fup = closure::Closure::wrap(Box::new(move |event: wasm_bindgen::JsValue| {
            let event = event
                .dyn_ref::<web_sys::KeyboardEvent>()
                .expect("no keyboard event?");

            f2.borrow_mut()(false, event)
        }) as Box<dyn FnMut(wasm_bindgen::JsValue)>);

        let body = utils::document().body().unwrap();
        body.set_onkeydown(Some(fdown.as_ref().unchecked_ref()));
        body.set_onkeyup(Some(fup.as_ref().unchecked_ref()));

        self.key_handlers = Some((fdown, fup));
    }

    pub fn deregister_all(&mut self) {
        if self.key_handlers.is_some() {
            let body = utils::document().body().unwrap();
            body.set_onkeydown(None);
            body.set_onkeyup(None);
        }

        for id in self.mouse_handlers.keys() {
            if let Ok(el) = utils::get_element_by_id::<web_sys::SvgElement>(id) {
                el.set_onclick(None);
            } else if let Ok(el) = utils::get_element_by_id::<web_sys::HtmlElement>(id) {
                el.set_onclick(None);
            } else {
                panic!("Cannot find appropriate element {:?}", id);
            }
        }
    }
}

#[derive(Clone)]
pub struct StateMachineHandle(Rc<RefCell<StateMachine>>);

impl StateMachineHandle {
    pub fn new(state_machine: &Rc<RefCell<StateMachine>>) -> StateMachineHandle {
        StateMachineHandle(state_machine.clone())
    }

    pub fn do_transition(&self, t: Transition) {
        StateMachine::do_transition(&self.0, t)
    }

    pub fn do_event(&self, event: Event) {
        let t = StateMachine::active(&self.0).event(event);
        self.do_transition(t)
    }

    pub fn do_cheat(&self, cheat: crate::cheats::CheatCommand) {
        self.do_event(Event::Cheat(cheat))
    }

    pub fn active_state(&self) -> std::cell::RefMut<Box<dyn State>> {
        std::cell::RefMut::map(StateMachine::active(&self.0), |storage| &mut storage.state)
    }
}

pub struct StateInitializer<'a> {
    handle: StateMachineHandle,
    event_handlers: &'a mut EventHandlers,
}

impl<'a> StateInitializer<'a> {
    fn new(
        state_machine: &Rc<RefCell<StateMachine>>,
        event_handlers: &'a mut EventHandlers,
    ) -> StateInitializer<'a> {
        StateInitializer {
            handle: StateMachineHandle::new(state_machine),
            event_handlers,
        }
    }

    pub fn register_onclick(&mut self, id: &str) {
        let handle = self.handle.clone();
        let id2 = id.to_owned();
        let canvas = utils::get_element_by_id::<web_sys::HtmlCanvasElement>("game-canvas").unwrap();
        self.event_handlers.register_onclick(id, move |event| {
            let click = nalgebra::Point2::new(event.client_x() as f64, event.client_y() as f64);
            let canvas_click = utils::screen_to_contain_canvas(&canvas, click);
            let canvas_click = nalgebra::Point2::new(canvas_click.x as f32, canvas_click.y as f32);
            handle.do_event(Event::MouseClick {
                target: &id2,
                coords: canvas_click,
            });
        });
    }

    pub fn register_keyevents(&mut self) {
        let handle = self.handle.clone();
        self.event_handlers
            .register_keyevents(move |keydown, event| {
                if keydown {
                    handle.do_event(Event::KeyDown(&event.key()));
                } else {
                    handle.do_event(Event::KeyUp(&event.key()));
                }
            });
    }

    pub fn get_handle(&self) -> StateMachineHandle {
        self.handle.clone()
    }
}

/// "Container" for a stored state
struct StateStorage {
    pub state: Box<dyn State>,
    pub do_loop: Option<bool>,
    pub event_handlers: EventHandlers,
}

impl StateStorage {
    pub fn new<S: State + 'static>(state: S) -> StateStorage {
        StateStorage::with_boxed(Box::new(state))
    }

    pub fn with_boxed(state: Box<dyn State>) -> StateStorage {
        StateStorage {
            state,
            do_loop: None,
            event_handlers: EventHandlers::new(),
        }
    }

    // ----------

    pub fn init(&mut self, sm: &Rc<RefCell<StateMachine>>) -> Transition {
        self.state
            .init(StateInitializer::new(sm, &mut self.event_handlers))
    }

    pub fn deinit(&mut self) {
        self.state.deinit();
        self.event_handlers.deregister_all();
    }

    pub fn update(&mut self, timestamp: f64) -> Transition {
        self.state.update(timestamp)
    }

    fn event(&mut self, event: Event) -> Transition {
        self.state.event(event)
    }
}

pub struct StateMachine {
    states: Vec<StateStorage>,
    loop_handler: Option<closure::Closure<dyn FnMut(f64)>>,
    loop_scheduled: bool,
}

impl StateMachine {
    pub fn launch<S: State + 'static>(initial: S) {
        let this = Rc::new(RefCell::new(StateMachine {
            states: vec![StateStorage::new(initial)],
            loop_handler: None,
            loop_scheduled: false,
        }));

        crate::cheats::initialize_cheats(StateMachineHandle::new(&this));

        // Sadly Rc::new_cyclic() isn't stable yet ...
        let loop_handler = {
            let this = this.clone();
            closure::Closure::wrap(Box::new(move |timestamp| {
                {
                    let mut sm = this.borrow_mut();
                    if !sm.loop_scheduled {
                        // Running without being scheduled??
                        crate::console_warn!("Loop handler running without being scheduled!");
                        return;
                    }
                    sm.loop_scheduled = false;
                }

                // First, check if the event is still wanted by the current state
                let t = {
                    let mut active = StateMachine::active(&this);
                    if active.do_loop != Some(true) {
                        // If not, just pretend nothing ever happened.
                        return;
                    }

                    active.update(timestamp)
                };

                StateMachine::do_transition(&this, t);
            }) as Box<dyn FnMut(f64)>)
        };
        this.borrow_mut().loop_handler = Some(loop_handler);

        let t = StateMachine::active(&this).init(&this);
        StateMachine::do_transition(&this, t);
    }

    fn active(this: &Rc<RefCell<StateMachine>>) -> std::cell::RefMut<StateStorage> {
        std::cell::RefMut::map(this.borrow_mut(), |sm| sm.states.last_mut().unwrap())
    }

    pub fn do_event(this: &Rc<RefCell<StateMachine>>, event: Event) {
        let t = StateMachine::active(this).event(event);
        StateMachine::do_transition(this, t);
    }

    pub fn do_transition(this: &Rc<RefCell<StateMachine>>, t: Transition) {
        // Iteratively perform all transitions until there is no more left to do and hopefully
        // events/loop frames are scheduled.
        let mut next = Some(t);
        while let Some(t) = next {
            next = match t {
                Transition::Keep => {
                    if StateMachine::active(this)
                        .do_loop
                        .expect("no previous `Loop` or `Sleep` transition")
                    {
                        Some(Transition::Loop)
                    } else {
                        Some(Transition::Sleep)
                    }
                }
                Transition::Loop => {
                    use wasm_bindgen::JsCast;
                    StateMachine::active(this).do_loop = Some(true);

                    // Only schedule if a loop was not already scheduled.  Otherwise we'll end up
                    // with a lot of requestAnimationFrame() loops running in parallel.
                    if !this.borrow().loop_scheduled {
                        let mut sm = this.borrow_mut();
                        let loop_handler = sm.loop_handler.as_ref().unwrap();
                        utils::window()
                            .request_animation_frame(loop_handler.as_ref().unchecked_ref())
                            .unwrap();
                        sm.loop_scheduled = true;
                    }
                    None
                }
                Transition::Sleep => {
                    StateMachine::active(this).do_loop = Some(false);
                    // We hope that at least some event handler/active promise is present which will
                    // wake us up again in the future. Otherwise, we'll get stuck from here ...
                    None
                }
                Transition::Replace(new) => {
                    let mut active = StateMachine::active(this);
                    active.deinit();
                    *active = StateStorage::with_boxed(new);
                    Some(active.init(this))
                }
                Transition::Push(new) => {
                    StateMachine::active(this).deinit();
                    this.borrow_mut().states.push(StateStorage::with_boxed(new));
                    Some(StateMachine::active(this).init(this))
                }
                Transition::Pop => {
                    StateMachine::active(this).deinit();
                    this.borrow_mut().states.pop().unwrap();
                    Some(StateMachine::active(this).init(this))
                }
            };
        }
    }
}
