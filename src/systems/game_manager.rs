use crate::resources;
use crate::utils;

#[legion::system]
pub fn update_game_manager(
    #[resource] clock: &resources::Clock,
    #[resource] game_manager: &mut resources::GameManager,
) {
    if let Some(timeout) = game_manager.return_timeout.as_mut() {
        let popup = match game_manager.game_over {
            true => "ingame-game-over",
            false => "ingame-return-to-heaven",
        };
        if game_manager.return_timestamp.is_none() {
            utils::get_element_by_id::<web_sys::SvgElement>(popup)
                .unwrap()
                .style()
                .set_property("display", "block")
                .unwrap();
        }

        let now = clock.wall_time();
        let timestamp = *game_manager.return_timestamp.get_or_insert(now);

        *timeout = now - timestamp;

        utils::get_element_by_id::<web_sys::Element>("ingame-return-secs")
            .unwrap()
            .set_inner_html(&format!("{:.0}", (3.0 - *timeout).ceil()));

        if game_manager.wants_return_to_heaven() {
            utils::get_element_by_id::<web_sys::SvgElement>(popup)
                .unwrap()
                .style()
                .set_property("display", "none")
                .unwrap();
        }
    }
}
