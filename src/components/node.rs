use crate::colors;
use crate::components;
use crate::resources;

pub struct Node {
    click_time: Option<f64>,
    ping_time: Option<f64>,
}

impl Node {
    pub fn new() -> Node {
        Node {
            click_time: None,
            ping_time: None,
        }
    }

    pub fn amount_completed(&self, time: f64) -> Option<f64> {
        self.click_time.map(|ct| ((time - ct) / 0.5).min(1.0))
    }

    pub fn click_now(&mut self, clock: &resources::Clock) {
        self.click_time.get_or_insert_with(|| clock.wall_time());
    }
}

#[legion::system(for_each)]
pub fn update_nodes(node: &mut Node, #[resource] clock: &resources::Clock) {
    use rand::Rng;
    let time = clock.wall_time();

    if node.click_time.is_none() && rand::thread_rng().gen_ratio(5, 1000) {
        node.click_time = Some(time);
    }

    if let Some(amount_completed) = node.amount_completed(time) {
        if amount_completed >= 1.0 {
            node.ping_time = Some(time);
            node.click_time = None;
        }
    }
    if let Some(ping_time) = node.ping_time {
        if (time - ping_time) > 4.0 {
            node.ping_time = None;
        }
    }
}

#[legion::system(for_each)]
pub fn draw_nodes(
    node: &Node,
    pos: &components::Position,
    #[resource] rendering: &mut resources::Rendering,
    #[resource] clock: &resources::Clock,
) {
    // TODO: Replace by proper API
    rendering.begin_path();
    rendering.set_fill_style(&colors::BACKGROUND);
    rendering.arc(
        pos.0.x as f64,
        pos.0.y as f64,
        10.0,
        0.0,
        std::f64::consts::TAU,
    );
    rendering.fill();

    rendering.begin_path();
    rendering.set_line_width(5.0);
    rendering.set_stroke_style(&colors::PRIMARY2_SHADE2);
    rendering.arc(
        pos.0.x as f64,
        pos.0.y as f64,
        10.0,
        0.0,
        std::f64::consts::TAU,
    );
    rendering.stroke();

    let time = clock.wall_time();
    let node_color = colors::PRIMARY2_SHADE1;
    if let Some(amount_completed) = node.amount_completed(time) {
        rendering.begin_path();
        rendering.set_stroke_style(&node_color);
        rendering.arc(
            pos.0.x as f64,
            pos.0.y as f64,
            18.0,
            0.0,
            std::f64::consts::TAU * amount_completed,
        );
        rendering.stroke();
    }

    if let Some(ping_time) = node.ping_time {
        let radius = (time - ping_time) * 180.0 + 18.0;
        let alpha = (1.0 - (time - ping_time) * 2.0).max(0.0).powi(2);
        let mut ring_color = node_color;
        ring_color.alpha = alpha;

        rendering.begin_path();
        rendering.set_stroke_style(&ring_color);
        rendering.arc(
            pos.0.x as f64,
            pos.0.y as f64,
            radius,
            0.0,
            std::f64::consts::TAU,
        );
        rendering.stroke();
    }
}
