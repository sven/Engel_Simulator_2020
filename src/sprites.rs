use crate::resources;
use crate::svg_loader;

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub enum Sprite {
    Player,
    BottleDropPointFull,
    BottleDropPointEmpty,
    TrojanHorse,
    NetworkSwitchConnected,
    NetworkSwitchDisconnected,
}

impl Sprite {
    pub fn filename(&self) -> &'static str {
        match *self {
            Sprite::Player => "player.svg",
            Sprite::BottleDropPointFull => "droppoint-full.svg",
            Sprite::BottleDropPointEmpty => "droppoint-empty.svg",
            Sprite::TrojanHorse => "trojan-horse.svg",
            Sprite::NetworkSwitchConnected => "switch-connected.svg",
            Sprite::NetworkSwitchDisconnected => "switch-disconnected.svg",
        }
    }

    pub async fn load_and_register(&self, renderer: &mut resources::Rendering) {
        let image_handle: resources::ImageHandle = (*self).into();
        if renderer.has_image(&image_handle) {
            return;
        }
        let filename = self.filename();
        let image_url = format!("resources/sprites/{}", filename);
        let image = svg_loader::load_svg_as_image(&image_url).await;
        renderer
            .register_image_on_handle(image, &image_handle)
            .expect("ImageHandle was registered twice - this should not happen");
    }

    pub async fn load_and_register_all(renderer: &mut resources::Rendering) {
        for sprite in &[
            Sprite::Player,
            Sprite::BottleDropPointFull,
            Sprite::BottleDropPointEmpty,
            Sprite::TrojanHorse,
            Sprite::NetworkSwitchConnected,
            Sprite::NetworkSwitchDisconnected,
        ] {
            sprite.load_and_register(renderer).await;
        }
    }
}

impl Into<resources::ImageHandle> for Sprite {
    fn into(self) -> resources::ImageHandle {
        resources::ImageHandle(self as usize)
    }
}
