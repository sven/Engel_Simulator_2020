use crate::resources;

#[legion::system]
pub fn camera(
    #[resource] rendering: &resources::Rendering,
    #[resource] camera: &resources::Camera,
) {
    let pos = -camera.position + camera.size / 2.0;
    // Rounded to nearest integer to prevent subpixel rendering overhead
    let (x, y) = (pos.x.round(), pos.y.round());
    rendering.set_transform(1.0, 0.0, 0.0, 1.0, x, y);
}
