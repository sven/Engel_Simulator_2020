pub struct Movable {
    pub velocity: nalgebra::Vector2<f32>,
}

impl Movable {
    pub fn new() -> Movable {
        Movable {
            velocity: nalgebra::Vector2::new(0.0, 0.0),
        }
    }
}
