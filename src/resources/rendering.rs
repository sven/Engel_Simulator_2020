use crate::utils::JsError;
use anyhow::Context;
use std::collections::HashMap;
use wasm_bindgen::JsCast;

fn get_hex_color_format(value: &str) -> u32 {
    let valid_characters = [
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'A', 'B',
        'C', 'D', 'E', 'F',
    ];
    let (hash_tag, value) = value.split_at(1);
    if hash_tag == "#" {
        value
            .chars()
            .map(|character| {
                if valid_characters.contains(&character) {
                    1
                } else {
                    0
                }
            })
            .fold(0, |acc, x| acc * x + 1)
    } else {
        0
    }
}

#[derive(Copy, Clone, Debug)]
pub struct Color {
    pub red: f64,
    pub green: f64,
    pub blue: f64,
    pub alpha: f64,
}

impl Color {
    pub fn with_rgba(red: f64, green: f64, blue: f64, alpha: f64) -> Self {
        Color {
            red,
            green,
            blue,
            alpha,
        }
    }
    pub fn with_rgb(red: f64, green: f64, blue: f64) -> Self {
        Color {
            red,
            green,
            blue,
            alpha: 1.0,
        }
    }
    pub fn with_hex_string(hex_string: &str) -> Option<Self> {
        match get_hex_color_format(hex_string) {
            3 => {
                let number = u16::from_str_radix(hex_string.get(1..4).unwrap(), 16).unwrap();
                let r = (number & 0xF00) >> 8;
                let g = (number & 0x0F0) >> 4;
                let b = number & 0x00F;
                Some(Self::with_rgb(
                    r as f64 / 15.0,
                    g as f64 / 15.0,
                    b as f64 / 15.0,
                ))
            }
            4 => {
                let number = u16::from_str_radix(hex_string.get(1..5).unwrap(), 16).unwrap();
                let r = (number & 0xF000) >> 12;
                let g = (number & 0x0F00) >> 8;
                let b = (number & 0x00F0) >> 4;
                let a = number & 0x000F;
                Some(Self::with_rgba(
                    r as f64 / 15.0,
                    g as f64 / 15.0,
                    b as f64 / 15.0,
                    a as f64 / 15.0,
                ))
            }
            6 => {
                let number = u32::from_str_radix(hex_string.get(1..7).unwrap(), 16).unwrap();
                let r = (number & 0xFF0000) >> 16;
                let g = (number & 0x00FF00) >> 8;
                let b = number & 0x0000FF;
                Some(Self::with_rgb(
                    r as f64 / 255.0,
                    g as f64 / 255.0,
                    b as f64 / 255.0,
                ))
            }
            8 => {
                let number = u32::from_str_radix(hex_string.get(1..9).unwrap(), 16).unwrap();
                let r = (number & 0xFF000000) >> 24;
                let g = (number & 0x00FF0000) >> 16;
                let b = (number & 0x0000FF00) >> 8;
                let a = number & 0x000000FF;
                Some(Self::with_rgba(
                    r as f64 / 255.0,
                    g as f64 / 255.0,
                    b as f64 / 255.0,
                    a as f64 / 255.0,
                ))
            }
            _ => None,
        }
    }

    pub fn as_color_string(&self) -> String {
        format!(
            "#{:02X}{:02X}{:02X}{:02X}",
            (self.red * 255.0) as u8,
            (self.green * 255.0) as u8,
            (self.blue * 255.0) as u8,
            (self.alpha * 255.0) as u8,
        )
    }
}

pub struct Rendering {
    ctx: web_sys::CanvasRenderingContext2d,
    images: HashMap<usize, web_sys::HtmlImageElement>,
    counter: usize,
}

#[derive(Copy, Clone, Eq, PartialEq)]
pub struct ImageHandle(pub usize);

impl Rendering {
    pub fn new(id: &str) -> anyhow::Result<Rendering> {
        let window = web_sys::window().context("Window was None")?;
        let canvas = window
            .document()
            .context("Document object not found")?
            .get_element_by_id(id)
            .context("Element not found")?;
        let canvas: web_sys::HtmlCanvasElement = canvas
            .dyn_into::<web_sys::HtmlCanvasElement>()
            .ok()
            .context("Couldn't convert element to canvas")?;
        let ctx = canvas
            .get_context("2d")
            .map_err(JsError::from)?
            .context("Could not get canvas context")?
            .dyn_into::<web_sys::CanvasRenderingContext2d>()
            .ok()
            .context("Couldn't convert element to CanvasRenderingContext2d")?;
        let images = HashMap::new();
        Ok(Rendering {
            ctx,
            images,
            counter: 10000,
        })
    }

    fn generate_image_handle(&mut self) -> ImageHandle {
        let id = self.counter;
        self.counter += 1;
        ImageHandle(id)
    }

    pub fn register_image(&mut self, image: web_sys::HtmlImageElement) -> ImageHandle {
        let image_handle = self.generate_image_handle();
        self.images.insert(image_handle.0, image);
        image_handle
    }

    pub fn register_image_on_handle(
        &mut self,
        image: web_sys::HtmlImageElement,
        image_handle: &ImageHandle,
    ) -> Result<(), anyhow::Error> {
        if self.has_image(image_handle) {
            Err(anyhow::anyhow!("ImageHandle is already registered"))
        } else {
            self.images.insert(image_handle.0, image);
            Ok(())
        }
    }

    pub fn deregister_image(
        &mut self,
        image_handle: &ImageHandle,
    ) -> Option<web_sys::HtmlImageElement> {
        self.images.remove(&image_handle.0)
    }

    pub fn has_image(&self, image_handle: &ImageHandle) -> bool {
        self.images.contains_key(&image_handle.0)
    }

    pub fn get_image(&self, image_handle: &ImageHandle) -> Option<web_sys::HtmlImageElement> {
        self.images.get(&image_handle.0).cloned()
    }

    pub fn get_image_size(&self, image_handle: &ImageHandle) -> Option<nalgebra::Vector2<f32>> {
        self.get_image(image_handle)
            .map(|image| nalgebra::Vector2::new(image.width() as f32, image.height() as f32))
    }

    pub fn draw_image(&self, handle: &ImageHandle, x: f64, y: f64) -> anyhow::Result<()> {
        let image = if let Some(image) = &self.get_image(handle) {
            image.clone()
        } else {
            return Err(anyhow::anyhow!("Image was not found"));
        };

        self.ctx
            .draw_image_with_html_image_element(&image, x, y)
            .map_err(JsError::from)?;
        Ok(())
    }

    pub fn set_fill_style(&self, color: &Color) {
        self.ctx.set_fill_style(&color.as_color_string().into());
    }
    pub fn set_stroke_style(&self, color: &Color) {
        self.ctx.set_stroke_style(&color.as_color_string().into());
    }

    pub fn fill_rect(&self, x: f64, y: f64, width: f64, height: f64) {
        self.ctx.fill_rect(x, y, width, height);
    }

    pub fn begin_path(&self) {
        self.ctx.begin_path();
    }

    pub fn close_path(&self) {
        self.ctx.close_path();
    }

    pub fn stroke(&self) {
        self.ctx.stroke();
    }

    pub fn arc(&self, x: f64, y: f64, radius: f64, start_angle: f64, end_angle: f64) {
        self.ctx.arc(x, y, radius, start_angle, end_angle).unwrap();
    }

    pub fn fill(&self) {
        self.ctx.fill();
    }

    pub fn set_line_width(&self, width: f64) {
        self.ctx.set_line_width(width);
    }

    pub fn move_to(&self, x: f64, y: f64) {
        self.ctx.move_to(x, y);
    }

    pub fn line_to(&self, x: f64, y: f64) {
        self.ctx.line_to(x, y);
    }

    pub fn set_transform(&self, a: f64, b: f64, c: f64, d: f64, e: f64, f: f64) {
        self.ctx.set_transform(a, b, c, d, e, f).unwrap();
    }
}
