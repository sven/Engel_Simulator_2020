pub struct GameManager {
    pub return_timeout: Option<f64>,
    pub return_timestamp: Option<f64>,
    pub game_over: bool,
}

impl GameManager {
    pub fn new() -> GameManager {
        GameManager {
            return_timeout: None,
            return_timestamp: None,
            game_over: false,
        }
    }

    pub fn request_return_to_heaven(&mut self) {
        self.return_timeout.get_or_insert(0.0);
    }

    pub fn request_game_over(&mut self) {
        self.game_over = true;
    }

    pub fn wants_return_to_heaven(&self) -> bool {
        self.return_timeout.map_or(false, |t| t >= 3.0)
    }

    pub fn wants_game_over(&self) -> bool {
        self.game_over
    }
}
