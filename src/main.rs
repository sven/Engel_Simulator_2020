use core::panic;
use wasm_bindgen::prelude::*;

pub mod angel_shifts;
pub mod cheats;
pub mod colliders;
pub mod colors;
pub mod components;
pub mod entities;
pub mod gamestate;
pub mod resources;
pub mod sprites;
pub mod states;
pub mod svg_loader;
pub mod systems;
pub mod utils;

#[wasm_bindgen]
extern "C" {
    type Error;

    #[wasm_bindgen(constructor)]
    fn new() -> Error;

    #[wasm_bindgen(structural, method, getter)]
    fn stack(error: &Error) -> String;
}

pub fn error_hook(panic_info: &panic::PanicInfo) {
    utils::get_first_element_by_class_name::<web_sys::HtmlElement>("crash-screen")
        .unwrap()
        .style()
        .set_property("display", "block")
        .unwrap();
    let mut message = panic_info.to_string();
    message.push_str("\n\nStacktrace:\n");
    let e = Error::new();
    let stack = e.stack();
    message.push_str(&stack);

    let url_encoded_message = js_sys::encode_uri_component(&format!("```\n{}\n```", &message))
        .as_string()
        .unwrap();
    let bugreport_link = format!("https://gitlab.muc.ccc.de/engel-simulator-2020/game/-/issues/new?issue[title]=Bug%20Report&issue[description]={}", url_encoded_message);
    utils::get_first_element_by_class_name::<web_sys::HtmlAnchorElement>(
        "crash-screen-bugreport-link",
    )
    .unwrap()
    .set_href(&bugreport_link);

    utils::get_first_element_by_class_name::<web_sys::HtmlElement>("crash-screen-error")
        .unwrap()
        .set_inner_html(&message);
}

#[wasm_bindgen]
pub fn start() -> Result<(), JsValue> {
    std::panic::set_hook(Box::new(error_hook));
    utils::update_version_info();
    gamestate::StateMachine::launch(states::MainMenuState::new());
    Ok(())
}
