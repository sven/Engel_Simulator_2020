use crate::svg_loader;

pub trait AngelShiftImpl {
    /// Return metadata about this shift.
    ///
    /// Used in heaven to display what task is up next.
    fn metadata(&self) -> ShiftMetadata;

    /// Return name of the selected level for this shift.
    fn level_name(&self) -> &str;

    /// Initialize the shift-specific bits of the game-world
    ///
    /// The world will already contain all common entities and resources.  The schedule is
    /// configured to run all "game world" systems like collision detection/movement before
    /// whatever is added here.  After the systems added here, rendering will be scheduled.
    fn init_gameworld(
        &self,
        world: &mut legion::World,
        resources: &mut legion::Resources,
        schedule_builder: &mut legion::systems::Builder,
        level: &svg_loader::SvgLevel,
    );
}

pub struct ShiftMetadata {
    /// Short name of this shift/task.
    pub title: String,
    /// Longer description of what needs to be done.
    pub description: String,
    /// Amount of hours that would be gained from working this shift.
    pub hours: usize,
}

pub struct AngelShift(pub Box<dyn AngelShiftImpl>);

impl AngelShift {
    pub fn metadata(&self) -> ShiftMetadata {
        self.0.metadata()
    }

    pub fn level_name(&self) -> &str {
        self.0.level_name()
    }

    pub fn init_gameworld(
        &self,
        world: &mut legion::World,
        resources: &mut legion::Resources,
        schedule_builder: &mut legion::systems::Builder,
        level: &svg_loader::SvgLevel,
    ) {
        self.0
            .init_gameworld(world, resources, schedule_builder, level);
    }
}
