pub struct Clock {
    last_tick: f64,
    frame_delta: f32,
    pub max_delta: f32,
}

impl Clock {
    pub fn new() -> Clock {
        Clock {
            frame_delta: 0.03,
            last_tick: 0.0,
            max_delta: 0.1,
        }
    }

    pub fn frame_delta(&self) -> f32 {
        self.frame_delta
    }

    pub fn update(&mut self, timestamp: f64) {
        let timestamp = timestamp / 1000.0;
        self.frame_delta = self.max_delta.min((timestamp - self.last_tick) as f32);
        self.last_tick = timestamp;
    }

    pub fn wall_time(&self) -> f64 {
        self.last_tick
    }
}
