use wasm_bindgen::closure;
use wasm_bindgen::prelude::*;

use anyhow::Context;
use wasm_bindgen::JsCast;

pub fn window() -> web_sys::Window {
    web_sys::window().expect("cannot access `window`")
}

pub fn document() -> web_sys::Document {
    window().document().expect("cannot access `document`")
}

#[macro_export]
macro_rules! console_log {
    ($($t:tt)*) => (::web_sys::console::log_1(&::std::format_args!($($t)*).to_string().into()))
}

#[macro_export]
macro_rules! console_warn {
    ($($t:tt)*) => (::web_sys::console::warn_1(&::std::format_args!($($t)*).to_string().into()))
}

fn contain(
    parent: nalgebra::Vector2<f64>,
    child: nalgebra::Vector2<f64>,
) -> (nalgebra::Vector2<f64>, nalgebra::Vector2<f64>) {
    let client_ratio = child.x / child.y;
    let parent_ratio = parent.x / parent.y;
    let mut child_resized = parent;

    if client_ratio > parent_ratio {
        child_resized.y = child_resized.x / client_ratio;
    } else {
        child_resized.x = child_resized.y * client_ratio;
    }

    ((parent - child_resized) / 2.0, child_resized)
}

pub fn screen_to_contain_canvas(
    canvas: &web_sys::HtmlCanvasElement,
    point: nalgebra::Point2<f64>,
) -> nalgebra::Point2<f64> {
    // Find location and size of the whole canvas
    let rect = canvas.get_bounding_client_rect();
    let rect_offset = nalgebra::Vector2::new(rect.x(), rect.y());
    let rect_size = nalgebra::Vector2::new(rect.width(), rect.height());

    // Find size of the canvas image
    let canvas_size = nalgebra::Vector2::new(canvas.width() as f64, canvas.height() as f64);

    // Calculate the fitted size of the canvas on screen
    let (fitted_offset, fitted_size) = contain(rect_size, canvas_size);

    // The canvas starts at these coordinates
    let screen_offset = rect_offset + fitted_offset;

    // Subtract the screen offset and scale by the size ratio:
    nalgebra::Point2::from(
        (point - screen_offset)
            .coords
            .component_mul(&canvas_size.component_div(&fitted_size)),
    )
}

pub fn get_element_by_id<T: wasm_bindgen::JsCast>(id: &str) -> anyhow::Result<T> {
    document()
        .get_element_by_id(id)
        .with_context(|| format!("cannot find element {:?}", id))?
        .dyn_into::<T>()
        .map_err(|_| anyhow::anyhow!("cannot cast into expected type"))
}

pub fn get_first_element_by_class_name<T: wasm_bindgen::JsCast>(class: &str) -> anyhow::Result<T> {
    document()
        .get_elements_by_class_name(class)
        .item(0)
        .with_context(|| format!("cannot find element {:?}", class))?
        .dyn_into::<T>()
        .map_err(|_| anyhow::anyhow!("cannot cast into expected type"))
}

pub fn event_handler<F, S>(setter: S, fun: F)
where
    F: FnMut(wasm_bindgen::JsValue) + 'static,
    S: FnOnce(&js_sys::Function),
{
    let f = closure::Closure::wrap(Box::new(fun) as Box<dyn FnMut(_)>);

    setter(f.as_ref().unchecked_ref());

    // Forget so the closure remains "live" forever and is not immediately de-allocated again.
    f.forget();
}

pub fn loop_animation_frame<F: FnMut() + 'static>(mut fun: F) {
    let request_animation_frame = |f: &closure::Closure<dyn FnMut()>| {
        web_sys::window()
            .expect("cannot open window")
            .request_animation_frame(f.as_ref().unchecked_ref())
            .expect("should register `requestAnimationFrame` OK");
    };

    let f = std::rc::Rc::new(std::cell::RefCell::new(None));
    let g = f.clone();

    *g.borrow_mut() = Some(closure::Closure::wrap(Box::new(move || {
        fun();

        // Schedule ourself for another requestAnimationFrame callback.
        request_animation_frame(f.borrow().as_ref().unwrap());
    }) as Box<dyn FnMut()>));

    request_animation_frame(g.borrow().as_ref().unwrap());
}

pub fn request_animation_frame<F: FnOnce() + 'static>(fun: F) {
    // Make sure that this closure can only ever be called once.
    let mut fun = Some(fun);
    let f = closure::Closure::wrap(Box::new(move || {
        if let Some(fun) = fun.take() {
            fun();
        }
    }) as Box<dyn FnMut()>);

    web_sys::window()
        .expect("cannot open window")
        .request_animation_frame(f.as_ref().unchecked_ref())
        .expect("should register `requestAnimationFrame` OK");

    // Forget so the closure remains "live" forever and is not immediately de-allocated again.
    f.forget();
}

#[derive(Debug)]
pub struct JsError(String);
impl std::fmt::Display for JsError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Javascript error: {}", self)
    }
}
impl std::error::Error for JsError {}
impl From<JsValue> for JsError {
    fn from(js_value: JsValue) -> JsError {
        JsError(format!("{:?}", js_value))
    }
}

pub fn update_version_info() {
    const VERSION: &str = env!("VERSION_INFO");
    let element: web_sys::Element = get_element_by_id("version-info").unwrap();
    element.set_inner_html(VERSION);
}
