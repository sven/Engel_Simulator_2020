use crate::gamestate;
use crate::states;
use crate::utils;

pub struct GameOverState {
    gui_svg: web_sys::SvgElement,
}

impl GameOverState {
    pub fn new() -> GameOverState {
        GameOverState {
            gui_svg: utils::get_element_by_id("game-over-ui").unwrap(),
        }
    }
}

impl gamestate::State for GameOverState {
    fn init(&mut self, mut init: gamestate::StateInitializer) -> gamestate::Transition {
        init.register_onclick("game-over-replay");
        init.register_onclick("game-over-quit");
        self.gui_svg
            .style()
            .set_property("display", "block")
            .unwrap();

        gamestate::Transition::Sleep
    }

    fn deinit(&mut self) {
        self.gui_svg
            .style()
            .set_property("display", "none")
            .unwrap();
    }

    fn event(&mut self, event: gamestate::Event) -> gamestate::Transition {
        match event {
            gamestate::Event::MouseClick {
                target: "game-over-replay",
                ..
            } => gamestate::Transition::push(states::HeavenState::new(None)),
            gamestate::Event::MouseClick {
                target: "game-over-quit",
                ..
            } => gamestate::Transition::Pop,
            event => {
                crate::console_warn!("unknown event {:?}", event);
                gamestate::Transition::Keep
            }
        }
    }
}
