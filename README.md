Ever wanted to feel like an angel?  Are you sad that you can't do your annual
shift of wristband controls this year?  Fear not, we have exactly what you need!

Introducing:

![Engel Simulator 2020](misc/header.png)

**Engel Simulator 2020** is an upcoming rogue-like title where you collect
angel-hours to finally get that t-shirt you've always wanted (_t-shirt not
actually included_)!  Dive into a **realistic** and **immersive** (_2D
vector-art_) congress world and feel just like you did back when leaving the
house wasn't a crime.  We've got a wide range of angel-shifts (_TODO .._) that
**feel just like the real deal**.  But be careful to not loose your sanity,
because you'll be facing _dangerous_ obstacles:  From _eardrum-rupturing music_
to _tinfoil-hat-wearing conspiracy theorists_, we have got it all!

Do you have what it takes to get an angel shirt?  Play now at

[![https://engel-simulator.club](misc/website.png)](https://engel-simulator.club)

#### Disclaimer
... Yeah, we started working on this way too late and there's still lots of
features to work on.  If you want to help, please get in touch!  We're reachable
via

- **IRC**: `#rust` on `darkfasel.net`
- **Matrix**: `#rust:darkfasel.net`
- Open an issue here in this GitLab project

# Hacking
- [Project Structure](#project-structure)
- [Build Dependencies](#build-dependencies)
- [Development Environment](#development-environment)
- [Level Design](#level-design)

## Project Structure
The project is mainly split into two parts:  The game logic, written in Rust,
living in `src/`, and the web-frontend in `www/src/` which is mainly HTML and
SCSS + a bit of JS glue code.  Resources like sprites and levels live in
`www/resources/`.  Additionally, some custom helper JavaScript (TypeScript
actually) functions which are called from Rust are defined in `rust-web-modules/`.

#### `src/` - Rust Game Logic
We're using the [`legion`](https://github.com/amethyst/legion) ECS framework for
the in-game world and a custom state-machine for managing the overall gamestate.

- `src/utils.rs`: Javascript-Gluecode, e.g. a `document.getElementById()`
  wrapper.
- `src/colliders.rs`: `ncollide2d` integration
- `src/components/`: Components for game entities
- `src/resources/`: Game-global resources like a global clock or the canvas
  rendering proxy.
- `src/systems/`: ECS systems for the game.
- `src/entities/`: Entity factories for instanciation different kinds of
  entities.
- `src/gamestate.rs`: Game state machine implementation.
- `src/states/`: Game-states like _Heaven_, _In-Game_, _Main-Menu_, etc.  The
  most important one is probably `src/states/ingame.rs` which is where all the
  ECS magic happens.
- `src/angel_shifts/`: Implementations of the different angel shifts.

#### `rust-web-modules/` - TypeScript helpers for Rust
Some things are just super ugly to do in Rust directly (anything related to the
DOM) so we have a few helper functions written in TypeScript to "offload" this
work.

#### `www/src/` - Web Frontend
- `www/src/index.html`: Main HTML Document
- `www/src/styles.scss`: Stylesheet using Sass
- `www/src/_rc3-*`: Styles and Fonts for RC3

#### `www/resources/` - Sprites and Levels
- `www/resources/levels/` - Maps, see [Level Design](#level-design) for all the
  gory details.
- `www/resources/sprites/` - Sprites for in-game entities.  Sprites are loaded
  via entries in `src/sprites.rs`.

## Build Dependencies
- `rustc` & `cargo` **latest stable** (=1.48.0 as of now).  Best install them via [`rustup`] ...
- [`wasm-pack`]
- `npm`
- (optional: [`cargo-watch`])

[`wasm-pack`]: https://github.com/rustwasm/wasm-pack
[`rustup`]: https://rustup.rs/
[`cargo-watch`]: https://github.com/passcod/cargo-watch

With those dependencies installed, run

```bash
cd www/ && npm install
```

## Development Environment
Open two shell sessions, one for the rust build and one for the webpack dev
server:

```bash
# For Rust:
cargo watch -s "wasm-pack build --dev" --watch src
# or, if you don't use cargo-watch, just run
wasm-pack build --dev

# For the webpack dev server:
cd www/
npm run rust-web-modules
npm run start
```

Then point your browser to `http://localhost:8080` and start hacking!  The
website will automatically reload when any changes are done to the web sources
(`www/`) or the Rust sources (`src/`).  If you change the `rust-web-modules/`
you'll have to manually run `npm run rust-web-modules` again (please fix!).

## Level Design
We're using an _Inkscape SVG_ file as the level format (yes, the inkscape
specific tags are important) and thus we use [Inkscape](https://inkscape.org/)
as the level editor.  **There are a lot of pitfalls in designing levels so**
**please read this guide carefully!**

### Colors
We have created an Inkscape palette with the RC3 colors for your convenience.
You can find it in [`misc/rc3-palette.gpl`](misc/rc3-palette.gpl).  To use it,
copy this file to `~/.config/inkscape/palettes/rc3-palette.gpl`.  Then, select
it in Inkscape in the bottom right corner like this:

![select palette in Inkscape](misc/select-palette.png)

To use the colors, **click** the swatches to set _fill-color_ for the currently
selected objects or **shift-click** to set _stroke-color_.

Please use the last color of the palette (`#100e23`) as the level background.

### Layers
The level contains a number of layers which the game-engine expects to exist:

- `background`: Any graphic elements to be shown _below_ the player and game
  objects.
- `collider`: A special layer that is not shown in-game which defines static map
  colliders.  There are a lot of restrictions in this layer, please see [Map
  Colliders](#map-colliders-collider-layer) for details.
- `spawns`: Spawnpoints for game objects.  This layer is also not shown and has
  special semantics.  See [Spawnpoints](#spawnpoints-spawns-layer).
- `foreground`: Any graphic elements to be drawn _on top of_ the player and
  game-objects.

### Map Colliders: `collider` Layer
The `collider` layer defines static colliders for the map.  Only the following
SVG elements may appear in this layer:

- `<rect />`: A rectangle collider (may be rotated).  Created in Inkscape using
  the _Rectangle_ tool (shortcut: `R`).
- `<circle />`: A circular collider.  Created in Inkscape using the _Ellipse_
  tool (shortcut: `E`).  **Careful**: Keep `CTRL` pressed while creating the
  circle, otherwise you'll get an `<ellipse />` which is not supported.

For consistency, please color all colliders in 50% transparent red (`#ff000080`)
with no stroke.

### Spawnpoints: `spawns` Layer
The `spawns` layer defines all possible spawn points for game entities.  This
layer should only contain SVG `<text />` elements.  The origin point for the
text object describes the spawnpoint, the "text" describes _what_ can be
spawned.  The following spawnpoints are currently known:

- `player`: Defines a possible player spawn location.  One will be randomly
  selected when entering the level.
- `bottledrop`: Defines a possible location for a bottle drop point.  Currently,
  4 will be randomly selected during level load.

Please use the `monospace` font and white text color for consistency.

---

# License
Licensed under the GNU General Public License v3.0 ([COPYING](COPYING) or
<https://www.gnu.org/licenses/gpl-3.0-standalone.html>).

## Contribution
Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, shall be licensed as above, without any
additional terms or conditions.
