let loadingSequence = 0;

export async function load_svg(url: string): Promise<string> {
  const response = await fetch(url);
  const svgText = await response.text();
  const loadingDiv = document.createElement("div");
  const loadingDivId  = `loading-div-${loadingSequence}`
  loadingDiv.setAttribute("id", loadingDivId);
  loadingDiv.style.display = "hidden";
  loadingSequence += 1;
  loadingDiv.innerHTML = svgText;
  document.body.appendChild(loadingDiv);
  return loadingDivId;
}

export function clean_svg(loadingId: string) {
  document.getElementById(loadingId).remove();
}

export function extract_svg_code(loadingId: string): string | null {
  const loadingDiv = document.getElementById(loadingId);
  if (!loadingDiv) {
    return null;
  }
  return loadingDiv.innerHTML;
}

export function hide_canvas_node_by_query(loadingId: string, query: string) {
  document.getElementById(loadingId).querySelectorAll(query)
    .forEach((elment: HTMLElement) => elment.style.display = "none");
}

export function show_canvas_node_by_query(loadingId: string, query: string) {
  document.getElementById(loadingId).querySelectorAll(query)
    .forEach((elment: HTMLElement) => elment.style.display = "block");
}

export function convert_svg_to_html_image(loadingId: string): Promise<HTMLImageElement> {
  return new Promise((success, fail) => {
    const image = new Image();
    const base64Canvas = "data:image/svg+xml;base64," + btoa(extract_svg_code(loadingId));
    if (base64Canvas === null) {
      return fail();
    }
    image.onload = () => {
      success(image);
    };
    image.onerror = event => {
      fail(event);
    }
    image.src = base64Canvas;
  });
}

export function convert_svg_to_image(loadingId: string): Promise<HTMLImageElement> {
  return new Promise((success, fail) => {
    convert_svg_to_html_image(loadingId)
      .then(svgImage => {
        const canvas = document.createElement("canvas") as HTMLCanvasElement;
        canvas.width = svgImage.width;
        canvas.height = svgImage.height;
        const image = new Image();
        const ctx = canvas.getContext("2d");
        ctx.drawImage(svgImage, 0, 0);
        image.onload = () => {
          success(image);
        };
        image.onerror = event => {
          fail(event);
        };
        image.src = canvas.toDataURL("image/png");
    }).catch(e => fail(e));
  });
}